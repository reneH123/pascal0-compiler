1. grammar:
ok 1_0.p0..1_8.p0 positive

2. grammar:
ok 2_1.p0..2_15.p0 positive
ok 2_16.p0..2_23.p0 negative

3. grammar:
ok 3_1.p0..3_14.p0 positive
ok 3_15..3_19.p0 negative

4. grammar:
ok 4_1.p0..4_4.p0 positive
ok 4_5.p0..4_12.p0 negative

5.grammar:
(already covered by 4th grammar)

6. grammar:
ok 6_1.p0..6_3.p0 positive
ok 6_4.p0 negative

7. grammar:
ok 7_1.p0..7_26.p0 positive
ok 7_27.p0..7_41.p0 negative

8. grammar:
ok 8_1.p0..8_27.p0 positive
ok 8_28.p0.. negative

9. grammar
ok 9_1.p0..9_27.p0 positive

10. grammar:
ok 10_1.p0..10_6.p0 positive
ok 10_7.p0..10_11.p0 negative

11.grammar:
ok 11_1.p0..11_8.p0 positive

1 transition 3 grammar:
ok 1t3_1.p0..1t3_12.p0 positive
ok 1t3_13.p0..1t3_17.p0 negative

1 transition 2 grammar:
ok 1t2_1.p0..1t2_19.p0 positive

1 transition 11 grammar:
ok 1t11_1.p0..1t11_9.p0 positive

1->2 transition 7 grammar:
ok 12t7_1.p0..12t7_26.p0 positive
