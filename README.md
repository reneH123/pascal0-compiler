Rene Haberland, 2004-2005 Saint Petersburg  (haberland1@mail.ru)

This is a fully-handwritten lexer and parser for a Pascal-subset written in Pascal, called Pascal0 within a stand-alone university project.

Both syntax analyzers are written in Pascal (as MS DOS-application). Although it is not making extensive use of tables at all, the memory restrictions due to 64K are severe and it is almost entirely needed and there is not really much space for improvement.


The whole software was written by me, and is licensed under Creative Commons SA-NonCommercial version 3.

